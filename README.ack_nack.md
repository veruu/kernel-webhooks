# ACK/NACK Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ack_nack \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
	--linux-dir /usr/src/linux

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--linux-dir` argument can also be passed to the script via the `LINUX_DIR`
environment variable.
