"""Webhook interaction tests."""
import unittest
from unittest import mock

import webhook.ack_nack


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestPublic(unittest.TestCase):
    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_get_required_reviewers(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n'
        get_maint.return_value = (0, maint_stdout)

        reviewers = webhook.ack_nack._get_required_reviewers(['mocked'], 'mocked')
        self.assertEqual(reviewers, set(['user1@redhat.com', 'user2@redhat.com']))

    def test_parse_tag(self):
        tag = webhook.ack_nack._parse_tag('Acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('LGTM!\n\nAcked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Rescind-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-acked-by: User 1 <user1@redhat.com>')
        self.assertEqual(tag, ('Revoke-acked-by', 'User 1', 'user1@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Rescind-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Rescind-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Revoke-nacked-by: User 2 <user2@redhat.com>')
        self.assertEqual(tag, ('Revoke-nacked-by', 'User 2', 'user2@redhat.com'))

        tag = webhook.ack_nack._parse_tag('Unknown-tag: User 3 <user3@redhat.com>')
        self.assertIsNone(tag[0])

        tag = webhook.ack_nack._parse_tag('You forgot your Acked-by: User 1 <user1@redhat.com>')
        self.assertIsNone(tag[0])

    def test_process_acks_nacks(self):
        msgs = []
        msgs.append('Some comment')
        msgs.append('Another comment')
        msgs.append('Acked-by: User 1 <user1@redhat.com>')
        msgs.append('Yet another comment')
        msgs.append('Acked-by: User 2 <user2@redhat.com>')
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs)
        self.assertEqual(acks, set([('User 1', 'user1@redhat.com'),
                                    ('User 2', 'user2@redhat.com')]))
        self.assertEqual(nacks, set([]))

        msgs = []
        msgs.append('Some comment')
        msgs.append('Another comment')
        msgs.append('Acked-by: User 1 <user1@redhat.com>')
        msgs.append('Yet another comment')
        msgs.append('Nacked-by: User 2 <user2@redhat.com>')
        msgs.append('Rescind-acked-by: User 1 <user1@redhat.com>')
        (acks, nacks) = webhook.ack_nack._process_acks_nacks(msgs)
        self.assertEqual(acks, set([]))
        self.assertEqual(nacks, set([('User 2', 'user2@redhat.com')]))

    def test_get_ack_nack_summary(self):
        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([], [], set([])),
                         ('Missing', 'Requires 2 more ACKs.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com')],
                                                                [], set([])),
                         ('Missing', 'ACKed by User 1 <user1@redhat.com>. Requires 1 more ACKs.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set([])),
                         ('OK', 'ACKed by User 1 <user1@redhat.com>, User 2 <user2@redhat.com>.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set(['user1@redhat.com',
                                                                         'user2@redhat.com'])),
                         ('OK',
                          'ACKed by User 1 <user1@redhat.com>*, User 2 <user2@redhat.com>*. ' +
                          'Code owners are marked with a *.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [], set(['user1@redhat.com',
                                                                         'user3@redhat.com'])),
                         ('Missing',
                          'ACKed by User 1 <user1@redhat.com>*, User 2 <user2@redhat.com>. ' +
                          'Requires at least 1 ACK(s) from someone in the set ' +
                          'user3@redhat.com*. Code owners are marked with a *.'))

        self.assertEqual(webhook.ack_nack._get_ack_nack_summary([('User 1', 'user1@redhat.com'),
                                                                 ('User 2', 'user2@redhat.com')],
                                                                [('User 3', 'user3@redhat.com')],
                                                                set(['user1@redhat.com',
                                                                     'user2@redhat.com'])),
                         ('NACKed',
                          'ACKed by User 1 <user1@redhat.com>*, User 2 <user2@redhat.com>*. ' +
                          'NACKed by User 3 <user3@redhat.com>. Code owners are marked with a *.'))

    @mock.patch('webhook.ack_nack._run_get_maintainers')
    def test_process_merge_request(self, get_maint):
        maint_stdout = 'User 1 <user1@redhat.com> (maintainer:SUBSYSTEM)\n' + \
                       'User 2 <user2@redhat.com> (maintainer:SUBSYSTEM)\n'
        get_maint.return_value = (0, maint_stdout)

        msgs = []
        msgs.append(self.__create_note_body('Some comment'))
        msgs.append(self.__create_note_body('Another comment'))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>'))
        msgs.append(self.__create_note_body('Yet another comment'))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>'))

        gl_mergerequest = mock.Mock()
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}]}

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            webhook.ack_nack.process_merge_request(gl_mergerequest, 'mocked')
            self.assertIn("ACK/NACK Summary: OK - ACKed by ", logs.output[-1])

    def __create_note_body(self, body):
        ret = mock.Mock()
        ret.body = body
        return ret
