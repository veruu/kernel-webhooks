"""Webhook interaction tests."""
import copy
import json
import os
import unittest
from unittest import mock

import webhook.common
import webhook.public


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestPublic(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD = {'object_kind': 'merge_request',
               'project': {'id': 1,
                           'web_url': 'https://web.url/g/p'},
               'object_attributes': {'iid': 2}
               }

    CHANGES = {"labels": [], "changes": [{
        "old_path": "/dev/null",
        "new_path": "redhat/configs/ark/generic/CONFIG_FOO",
        "a_mode": "100644",
        "b_mode": "100644",
        "diff":
        # pylint: disable=line-too-long
        r"--- /dev/null\ +++ b/redhat/configs/ark/generic/CONFIG_FOO\ @@ -0,0 +1 @@\ +# CONFIG_FOO is not set",  # noqa: E501
        "new_file": True,
        "renamed_file": False,
        "deleted_file": False
        }],
        "description": "Cc: Joe User <juser@foo.com>",
    }

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'PUBLIC_ROUTING_KEYS': 'mocked', 'PUBLIC_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        consume_value = [(mock.Mock(), 'foo', json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.public.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""

        payload = copy.deepcopy(self.PAYLOAD)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload)

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD)

    @mock.patch('gitlab.Gitlab')
    def test_merge_request_update(self, mocked_gitlab):
        """Check a merge request update."""

        payload = copy.deepcopy(self.PAYLOAD)
        payload['object_attributes']['action'] = "update"

        self._test_payload(mocked_gitlab, True, payload)

    @mock.patch('gitlab.Gitlab')
    def test_drop_ack_nack_labels_update(self, mocked_gitlab):
        """Check dropping ack/nack labels."""

        payload = copy.deepcopy(self.PAYLOAD)
        payload['object_attributes']['action'] = "update"
        payload['object_attributes']['oldrev'] = "facedeadbeef"
        changes = copy.deepcopy(self.CHANGES)
        changes["changes"][0]["new_path"] = "VERSION"
        labels = ['foo', 'Acked-by: test1@test', 'Nacked-by: test2@test']
        assert_labels = ['foo']

        self._test(mocked_gitlab, True, payload, changes, [],
                   labels=labels, assert_labels=assert_labels)

    @mock.patch('gitlab.Gitlab')
    def test_apply_subsystem_label_generic(self, mocked_gitlab):
        """Check writing a generic subsystem label."""

        # expected new labels
        labels = ['Configuration', 'Subsystem: drivers', 'Subsystem: foo']
        search = [{"path": "drivers/foo/Kconfig"}]

        # disable function for now
        labels = []
        self._test_changes(mocked_gitlab, True, self.CHANGES, search,
                           assert_labels=labels)

    @mock.patch('gitlab.Gitlab')
    def test_apply_subsystem_label_network(self, mocked_gitlab):
        """Check writing a network subsystem label."""

        # expected new labels
        labels = ['Configuration', 'Subsystem: drivers',
                  'Subsystem: ethernet', 'Subsystem: net', 'Subsystem: foo']
        search = [{"path": "drivers/net/ethernet/foo/Kconfig"}]

        # disable function for now
        labels = []
        self._test_changes(mocked_gitlab, True, self.CHANGES, search,
                           assert_labels=labels)

    @mock.patch('gitlab.Gitlab')
    def test_no_label_to_apply(self, mocked_gitlab):
        """Check handling changes requiring no new labels."""

        changes = copy.deepcopy(self.CHANGES)
        changes["changes"][0]["new_path"] = "VERSION"

        self._test_changes(mocked_gitlab, True, changes, [])

    @mock.patch('gitlab.Gitlab')
    def test_remove_label_on_change(self, mocked_gitlab):
        """Check handling updated changes that should remove labels."""

        changes = copy.deepcopy(self.CHANGES)
        changes["changes"][0]["new_path"] = "VERSION"
        labels = ['foo', 'Configuration']
        assert_labels = ['foo']

        self._test_changes(mocked_gitlab, True, changes, [], labels,
                           assert_labels)

    @mock.patch('gitlab.Gitlab')
    def test_apply_no_ccs_label(self, mocked_gitlab):
        """Check No CCs label is applied correctly."""

        changes = copy.deepcopy(self.CHANGES)
        changes['description'] = "Just a simple description."
        labels = ['Configuration', 'No CCs']

        self._test_changes(mocked_gitlab, True, changes, [],
                           assert_labels=labels)

    @mock.patch('gitlab.Gitlab')
    def test_empty_description(self, mocked_gitlab):
        """Check an empty description is handled correctly."""

        changes = copy.deepcopy(self.CHANGES)
        changes['description'] = None
        labels = ['Configuration', 'No CCs']

        self._test_changes(mocked_gitlab, True, changes, [],
                           assert_labels=labels)

    def _test_payload(self, mocked_gitlab, result, payload):
        self._test(mocked_gitlab, result, payload, self.CHANGES, [])

    def _test_changes(self, mocked_gitlab, result, changes, search,
                      labels=None, assert_labels=None):
        self._test(mocked_gitlab, result, self.PAYLOAD, changes, search,
                   labels=labels, assert_labels=assert_labels)

    def _test(self, mocked_gitlab, result, payload, changes, search,
              labels=None, assert_labels=None):

        if not assert_labels:
            assert_labels = []
        if not labels:
            labels = []

        # setup dummy gitlab data
        projects = mock.Mock()
        commits = mock.Mock()
        merge_request = mock.Mock()
        mocked_gitlab().__enter__().projects.get.return_value = projects
        projects.mergerequests.get.return_value = merge_request
        projects.search.return_value = search
        merge_request.changes.return_value = changes
        merge_request.labels = labels

        # Let's keep the api to this function simple.  changes.description
        # matches what merge_request has.  So modify changes to update
        # merge_request.
        merge_request.description = changes['description']
        commits.message = changes['description']
        merge_request.commits.return_value = [commits]

        return_value = webhook.common.process_message('dummy', payload,
                                                      webhook.public.WEBHOOKS)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            projects.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
            if assert_labels:
                self.assertEqual(sorted(merge_request.labels),
                                 sorted(assert_labels))
        else:
            self.assertFalse(return_value)
