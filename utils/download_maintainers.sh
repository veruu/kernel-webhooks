#!/usr/bin/env bash

# This script downloads only the necessary files in order to run the
# get_maintainers.pl script so that an entire Linux source tree is
# not needed.

set -e

download_url()
{
	DEST="$1"
	MODE="$2"
	URL="$3"

	# Download the file into a temporary file within the destination
	# directory so that it'll be on the same filesystem as the destination.
	# These files will be actively in use so we want to eliminate any
	# potential race conditions.

	echo "Downloading ${URL} to ${DEST}"
	TMP=$(mktemp --tmpdir="${LINUX_DIR}")
	curl --silent -o "${TMP}" "${URL}"
	chmod "${MODE}" "${TMP}"
	mv "${TMP}" "${DEST}"
}

LINUX_DIR=${LINUX_DIR:-/usr/src/linux}
RHMAINTAINERS_URL=${RHMAINTAINERS_URL:-https://gitlab.com/cki-project/kernel-ark/-/raw/os-build/redhat/rhdocs/MAINTAINERS/RHMAINTAINERS}
GET_MAINTAINER_URL=${GET_MAINTAINER_URL:-https://gitlab.com/cki-project/kernel-ark/-/raw/os-build/scripts/get_maintainer.pl}

echo "Refreshing maintainers info in ${LINUX_DIR}"
mkdir -p "${LINUX_DIR}"
cd "${LINUX_DIR}"

# get_maintainers.pl expects some files and directories to be present but
# actually doesn't use them. Let's create some empty ones to keep it happy.
touch COPYING CREDITS Kbuild MAINTAINERS Makefile README .get_maintainer.conf
mkdir -p Documentation arch include drivers fs init ipc kernel lib \
         redhat/rhdocs/MAINTAINERS scripts

download_url redhat/rhdocs/MAINTAINERS/RHMAINTAINERS 0644 "${RHMAINTAINERS_URL}"
download_url scripts/get_maintainer.pl 0755 "${GET_MAINTAINER_URL}"
echo "--mpath redhat/rhdocs/MAINTAINERS --no-git --no-git-fallback" > .get_maintainer.conf
