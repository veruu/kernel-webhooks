"""Process all of the ACKs/NACKs that are associated with a merge request."""
import os
import pathlib
import subprocess
import sys

from cki_lib import logger
from cki_lib import misc

from . import common

LOGGER = logger.get_logger('cki.webhook.ack_nack')
MIN_REVIEWERS = 2


def _run_get_maintainers(changed_files, linux_dir):
    """Run the get_maintainers.pl script."""
    LOGGER.debug('Running get_maintainers.pl in directory %s', linux_dir)
    # The get_maintainers.pl script requires the file to exist, so create some
    # empty files as placeholders. It's OK to leave these in place.
    for changed_file in changed_files:
        dirname = os.path.dirname(changed_file)
        os.makedirs(os.path.join(linux_dir, os.path.join(dirname)),
                    exist_ok=True)
        pathlib.Path(os.path.join(linux_dir, changed_file)).touch()

    os.chdir(linux_dir)
    torun = ['./scripts/get_maintainer.pl', '-f'] + changed_files
    ret = subprocess.run(torun, capture_output=True, check=True, text=True)
    LOGGER.debug('Executed %s, return code=%s, stdout=%s',
                 torun, ret.returncode, ret.stdout)

    return (ret.returncode, ret.stdout)


def _save(gl_mergerequest, status, message):
    note = f'ACK/NACK Summary: {status} - {message}'
    LOGGER.info(note)

    if misc.is_production():
        # Note: Temporary save logic. This will be replaced with separate logic that updates a
        # comment that's shared between multiple webhooks in GitLab.
        gl_mergerequest.labels.append(f'ack_nack::{status}')
        gl_mergerequest.save()
        gl_mergerequest.notes.create({'body': note})


def _get_required_reviewers(changed_files, linux_dir):
    """Parse the output from the get_maintainers.pl script and returns list of email addresses."""
    (returncode, stdout) = _run_get_maintainers(changed_files, linux_dir)
    if returncode != 0:
        LOGGER.error('Error running get_maintainers.pl (%s): %s', returncode, stdout)
        # Add a bogus required reviewer so that the merge request can't be approved
        return ['ERROR_CHECK_SCRIPT_OUTPUT']

    reviewers = set([])
    for line in stdout.split('\n'):
        if not line:
            continue

        reviewers.add(line.split('<')[1].split('>')[0])

    return reviewers


def _parse_tag(message):
    """Parse an individual message and look for any tags of interest."""
    for tag in ['Acked-by', 'Nacked-by', 'Rescind-acked-by', 'Revoke-acked-by',
                'Rescind-nacked-by', 'Revoke-nacked-by']:
        for line in message.split('\n'):
            search_tag = f'{tag}: '
            if not line.startswith(search_tag):
                continue

            name_email = message.split(search_tag)[1].split(' <')
            name = name_email[0]
            email = name_email[1].split('>')[0]

            return (tag, name, email)

    return (None, None, None)


def _process_acks_nacks(messages):
    """Process a list of messages and collect any ACKs/NACKS."""
    acks = set([])
    nacks = set([])
    for message in messages:
        (tag, name, email) = _parse_tag(message)
        if not tag:
            continue

        LOGGER.debug('Processing %s %s <%s>', tag, name, email)

        name_email = (name, email)
        if tag == 'Acked-by':
            acks.add(name_email)
        elif tag == 'Nacked-by':
            nacks.add(name_email)
        elif tag in ('Rescind-acked-by', 'Revoke-acked-by'):
            if name_email in acks:
                acks.remove(name_email)
            else:
                LOGGER.warning('Cannot find ACK to revoke for %s', name_email)
        elif tag in ('Rescind-nacked-by', 'Revoke-nacked-by'):
            if name_email in nacks:
                nacks.remove(name_email)
            else:
                LOGGER.warning('Cannot find NACK to revoke for %s', name_email)
        else:
            LOGGER.warning('Internal error: Unsupported tag %s', tag)

    return (acks, nacks)


def _show_ack_nacks(ack_nacks, required_reviewers):
    num_required_reviewers = 0
    other_reviewers = set(required_reviewers)
    ret = ''

    for idx, ack_nack in enumerate(ack_nacks):
        if idx > 0:
            ret += ', '

        ret += f'{ack_nack[0]} <{ack_nack[1]}>'
        if ack_nack[1] in required_reviewers:
            ret += '*'
            num_required_reviewers += 1
            other_reviewers.remove(ack_nack[1])

    return (ret, num_required_reviewers, other_reviewers)


def _get_ack_nack_summary(acks, nacks, required_reviewers):
    footer = ['Code owners are marked with a *.'] if required_reviewers else []

    (ack_summary, num_required_reviewers, other_reviewers) = _show_ack_nacks(acks,
                                                                             required_reviewers)

    summary = []
    if ack_summary:
        summary.append(f'ACKed by {ack_summary}.')

    (nack_summary, _, _) = _show_ack_nacks(nacks, required_reviewers)
    if nack_summary:
        summary.append(f'NACKed by {nack_summary}.')
        return ('NACKed', ' '.join(summary + footer))

    min_required_reviewers = min(len(required_reviewers), MIN_REVIEWERS)
    if num_required_reviewers < min_required_reviewers:
        num_missing = min_required_reviewers - num_required_reviewers
        needs_more = f'Requires at least {num_missing} ACK(s) from someone in the set '
        for idx, reviewer in enumerate(other_reviewers):
            if idx > 0:
                needs_more += ', '
            needs_more += f'{reviewer}*'
        needs_more += '.'
        summary.append(needs_more)

        return ('Missing', ' '.join(summary + footer))

    if len(acks) < MIN_REVIEWERS:
        summary.append(f'Requires {MIN_REVIEWERS - len(acks)} more ACKs.')

        return ('Missing', ' '.join(summary + footer))

    return ('OK', ' '.join(summary + footer))


def process_merge_request(gl_mergerequest, linux_dir):
    """Process a merge request."""
    changed_files = \
        [change['new_path'] for change in gl_mergerequest.changes()['changes']]
    required_reviewers = _get_required_reviewers(changed_files, linux_dir)

    notes = [note.body for note in gl_mergerequest.notes.list(sort='asc', order_by='created_at')]
    (acks, nacks) = _process_acks_nacks(notes)

    LOGGER.debug('Changed files: %s', changed_files)
    LOGGER.debug('List of possible required reviewers: %s', required_reviewers)
    LOGGER.debug('List of ACKs: %s', acks)
    LOGGER.debug('List of NACKs: %s', nacks)

    summary = _get_ack_nack_summary(acks, nacks, required_reviewers)
    _save(gl_mergerequest, *summary)


def process_merge_request_webhook(gl_instance, msg, linux_dir):
    """Process a merge request message."""
    gl_project = gl_instance.projects.get(msg.payload['project']['id'])
    gl_mergerequest = gl_project.mergerequests.get(msg.payload['object_attributes']['iid'])
    process_merge_request(gl_mergerequest, linux_dir)


def get_webhooks(linux_dir):
    """Return the supported webhooks to listen on."""
    # The key should be the value GitLab uses in the object_kind parameter in
    # the payload of the web hook request.
    # https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
    return {
      'merge_request': lambda gl_instance, msg: process_merge_request_webhook(
          gl_instance, msg, linux_dir
      ),
    }


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('ACK_NACK')
    parser.add_argument('--linux-dir', **common.get_argparse_environ_opts('LINUX_DIR'),
                        help='Directory where get_maintainers.pl will be ran')
    args = parser.parse_args(args)

    if args.merge_request:
        _, gl_mr, _ = common.parse_mr_url(args.merge_request)
        process_merge_request(gl_mr, args.linux_dir)
        return

    common.consume_queue_messages(args, LOGGER, get_webhooks(args.linux_dir))


if __name__ == '__main__':
    main(sys.argv[1:])
