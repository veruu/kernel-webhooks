"""Query bugzilla for MRs."""
import enum
import json
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import common

LOGGER = logger.get_logger('cki.webhook.bugzilla')
SESSION = session.get_session('cki.webhook.bugzilla', LOGGER)


def filter_bz_line(line):
    """Filter out lines matching regex, but likely are NOT valid bug lines."""
    # Make sure the line starts with Bugzilla: to omit sentences
    # that might otherwise match our regexes
    if not line.startswith("Bugzilla:"):
        return ""

    # Filter out non-RH-bugzilla URLs
    http_re = re.compile(r'http', re.IGNORECASE)
    http = http_re.findall(line)
    brc_re = re.compile(r'bugzilla\.redhat\.com', re.IGNORECASE)
    brc = brc_re.findall(line)
    if http and not brc:
        return ""

    return line


def extract_all_bzs(message, mr_bugs, dependencies):
    """Extract all BZs from the message."""
    mlines = message.split('\n')
    bzs = []
    non_mr_bzs = []
    dep_bzs = []
    # Look for a string of 4-8 decimal digits on the line
    bznum_re = re.compile(r'\d{4,8}', re.IGNORECASE)

    try:
        iterator = iter(mlines)
    except TypeError:
        LOGGER.info("extract_all_bzs: mlines not iterable: %s", mlines)
        return (bzs, non_mr_bzs, dep_bzs)
    if iterator:
        pass

    for line in mlines:
        goodline = filter_bz_line(line)
        if not goodline:
            continue
        bugnumbers = bznum_re.findall(goodline)
        for bug in bugnumbers:
            if bug in dependencies:
                dep_bzs.append(bug)
            elif mr_bugs and bug not in mr_bugs:
                LOGGER.debug("Bugzilla: %s not listed in MR description", bug)
                non_mr_bzs.append(bug)
            else:
                bzs.append(bug)
    # We return empty arrays if no bugs are found
    return (bzs, non_mr_bzs, dep_bzs)


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y = extract_all_bzs(message, [], [])
    return bzs


def filter_dep_line(line):
    """Filter out lines that don't explicitly start with Depends:."""
    if not line.startswith("Depends:"):
        return ""

    # Filter out non-RH-bugzilla URLs
    http_re = re.compile(r'http', re.IGNORECASE)
    http = http_re.findall(line)
    brc_re = re.compile(r'bugzilla\.redhat\.com', re.IGNORECASE)
    brc = brc_re.findall(line)
    if http and not brc:
        return ""

    return line


def extract_dependencies(description):
    """Extract Depends: bugs from MR description."""
    dlines = description.split('\n')
    bzs = []
    # Look for a string of 4-8 decimal digits on the line
    bznum_re = re.compile(r'\d{4,8}', re.IGNORECASE)

    try:
        iterator = iter(dlines)
    except TypeError:
        LOGGER.info("extract_bzs: dlines not iterable: %s", dlines)
        return bzs
    if iterator:
        pass

    for line in dlines:
        goodline = filter_dep_line(line)
        if not goodline:
            continue
        bugnumbers = bznum_re.findall(goodline)
        for bug in bugnumbers:
            bzs.append(bug)
    # We return an empty array if there are no bz deps
    return bzs


def get_target_branch(project, merge_request):
    """Determine the target branch from merge request."""
    # Check what rhel minor branch the merge request is for. In case of the
    # main branch, it is the latest released minor + 1, we calculate it
    # based on the branches available in the repository. We use the minor
    # number to know which dist-git repository is our target for checking.
    major = "\\d+"
    glab_search = None
    basename = project.namespace["name"]
    match = re.search(r'^(\d+)\.', basename)
    if match:
        major = match.group(1)
        glab_search = "^%s." % (major)
    else:
        LOGGER.debug("validate_bzs: unable to get the major version")
    sre = f"^({re.escape(major)})\\.(\\d+)"
    major = 0
    minor = 0
    valid_target = False
    if merge_request.target_branch == "main":
        branches = project.branches.list(all=True, search=glab_search)
        bmain = -1
        sre += "$"
        regex = re.compile(sre)
        for branch in branches:
            match = regex.search(branch.name)
            if match:
                tmp = int(match.group(2))
                if tmp > bmain:
                    bmain = tmp
                    major = int(match.group(1))
                    valid_target = True
        minor = bmain + 1
    else:
        regex = re.compile(sre)
        match = regex.search(merge_request.target_branch)
        if match:
            major = int(match.group(1))
            minor = int(match.group(2))
            valid_target = True
        else:
            LOGGER.debug("validate_bzs: target branch not recognized")
    target_branch = f"rhel-{major}.{minor}.0"
    return (valid_target, target_branch)


def nobug_msg():
    """Return the message to use as a note for commits without bz."""
    msg = "These commits have no valid bzs, please add a line to the "
    msg += "the changelog in the format Bugzilla: <bz1>[,<bz2>,...,<bzn>]"
    return msg


def depbug_msg():
    """Return the message to use as note for commits that are dependencies."""
    msg = "These commits are for dependencies listed in the MR description. "
    msg += "Your MR cannot be merged until these dependencies are in main."
    return msg


def nomrbug_msg():
    """Return the message to note an unknown bug found in patches."""
    msg = "These commits contain a bug number that was not listed in the "
    msg += "merge request description. Please verify their correctness and "
    msg += "add them to your MR description as either bugs for this MR or "
    msg += "bugs on which this MR depends, if appropriate."
    return msg


def validate_bzs(mrstate, reviewed_items):
    """Validate bugzilla references."""
    (valid_target, target_branch) = get_target_branch(mrstate.project,
                                                      mrstate.merge_request)
    notes = []
    nobug_id = ""
    depbug_id = ""
    nomrbug_id = ""
    notgt_id = ""
    for bug in reviewed_items:
        bz_properties = {'approved': False, 'notes': [], 'logs': ''}
        reviewed_items[bug]["validation"] = bz_properties

        if not valid_target:
            notes += [] if notgt_id else ["Unknown repository target"]
            notgt_id = notgt_id if notgt_id else str(len(notes))
            bz_properties['notes'].append(notgt_id)
            continue

        if bug == "-":
            notes += [] if nobug_id else [nobug_msg()]
            nobug_id = nobug_id if nobug_id else str(len(notes))
            bz_properties['notes'].append(nobug_id)
            continue

        if bug in mrstate.deps:
            notes += [] if depbug_id else [depbug_msg()]
            depbug_id = depbug_id if depbug_id else str(len(notes))
            bz_properties['notes'].append(depbug_id)

        elif bug not in mrstate.mr_bugs:
            notes += [] if nomrbug_id else [nomrbug_msg()]
            nomrbug_id = nomrbug_id if nomrbug_id else str(len(notes))
            bz_properties['notes'].append(nomrbug_id)

        buginputs = {
            'package': 'kernel',
            'namespace': 'rpms',
            'ref': 'refs/heads/' + target_branch,
            'commits': [{
                'hexsha': 'HEAD',
                'files': ["kernel.spec"],
                'resolved': [int(bug)],
                'related': [],
                'reverted': [],
            }]
        }
        print(json.dumps(buginputs, indent=2))
        # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
        bugresults = SESSION.post(
            "https://dist-git.host.prod.eng.bos.redhat.com"
            "/lookaside/gitbz-query.cgi",
            json=buginputs)

        resjson = bugresults.json()
        print(json.dumps(resjson, indent=2))
        if resjson['result'] == 'ok':
            bz_properties['approved'] = True
        else:
            bz_properties['approved'] = False
            noteid = 0
            while noteid < len(notes):
                if notes[noteid] == resjson['error']:
                    break
                noteid += 1
            if noteid == len(notes):
                notes.append(resjson['error'])
            bz_properties['notes'].append(str(noteid+1))
        if mrstate.flags['bzreportlogs']:
            bz_properties['logs'] = resjson['logs']
    return (notes, target_branch)


def get_report_table(reviewed_items):
    """Create the table for the bugzilla report."""
    mr_approved = True
    table = []
    for bug in reviewed_items:
        bz_properties = reviewed_items[bug]["validation"]
        mr_approved = mr_approved and bz_properties['approved']
        commits = []
        for commit in reviewed_items[bug]["commits"]:
            commits.append(commit)
        table.append([bug, commits, bz_properties['approved'],
                      bz_properties['notes'], bz_properties['logs']])
    return (table, mr_approved)


def trim_notes(notes):
    """Trim/remove unwanted notes returned from dist-git cgi script."""
    noteid = 0
    while noteid < len(notes):
        trimmed = ""
        if notes[noteid] is None:
            noteid += 1
            continue
        strings = notes[noteid].split('\n')
        for string in strings:
            drop = False
            drop = string.startswith("*** No approved issue") or drop
            drop = string.startswith("*** Unapproved issue") or drop
            drop = string.startswith("*** Commit") or drop
            trimmed += "" if drop else string + '\n'
        notes[noteid] = trimmed
        noteid += 1


def print_target(merge_request, bz_target):
    """Print the bugzilla target if MR targets main branch."""
    target = merge_request.target_branch
    target = target if target != "main" else f"main ({bz_target})"
    return "Target Branch: " + target


def print_gitlab_report(mrstate, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for gitlab."""
    report = "<br>\n\n**BZ Readiness Report**\n\n"
    report += print_target(mrstate.merge_request, bz_target) + "   \n"
    report += " \n\n"
    report += "|BZ|Commits|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for row in table:
        bzn = f"BZ-{row[0]}" if row[0] != "-" else row[0]
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        report += "|"+bzn+"|"+"<br>".join(commits)
        report += "|"+str(row[2])+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request passes bz validation\n"
    else:
        report += "\nMerge Request fails bz validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bz approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    if mrstate.flags['bzreportlogs']:
        report += "LOGS:   \n"
        for row in table:
            report += row[4] + "   \n"

    return report


def print_text_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for the text console."""
    report = "\n\nBZ Readiness Report\n\n"
    report += print_target(merge_request, bz_target) + "\n\n"
    for row in table:
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0])+"|"+" ".join(commits)
        report += "|"+str(row[2])+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += "Merge Request %s bz validation\n" % status
    return report


def build_review_lists(mrstate, cid, review_lists, bug):
    """Build review_lists for this bug."""
    try:
        found_files = common.extract_files(mrstate.project.commits.get(cid), LOGGER)
    # pylint: disable=broad-except
    except Exception:
        found_files = []

    bugd = review_lists[bug] if bug in review_lists else {}
    commits = bugd["commits"] if "commits" in bugd else {}
    commits[cid] = found_files
    bugd["commits"] = commits
    return bugd


def check_on_bzs(mrstate):
    """Check BZs."""
    review_lists = {}
    mrdeps = False
    for commit in mrstate.merge_request.commits():
        commit = mrstate.project.commits.get(commit.id)
        # Skip merge commits, bugzilla link on the commit log for them
        # is not required and we do not care if bugs are listed on them
        if len(commit.parent_ids) > 1:
            continue
        try:
            found_bzs, non_mr_bzs, dep_bzs = extract_all_bzs(commit.message,
                                                             mrstate.mr_bugs,
                                                             mrstate.deps)
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
            non_mr_bzs = []

        for bug in found_bzs:
            review_lists[bug] = build_review_lists(mrstate, commit.id,
                                                   review_lists, bug)

        for bug in non_mr_bzs:
            review_lists[bug] = build_review_lists(mrstate, commit.id,
                                                   review_lists, bug)

        for bug in dep_bzs:
            review_lists[bug] = build_review_lists(mrstate, commit.id,
                                                   review_lists, bug)
            mrdeps = True

        if not found_bzs and not non_mr_bzs and not dep_bzs:
            review_lists["-"] = build_review_lists(mrstate, commit.id,
                                                   review_lists, "-")

    (notes, tgt) = validate_bzs(mrstate, review_lists)
    (table, approved) = get_report_table(review_lists)
    return (notes, tgt, table, approved, mrdeps)


class State(enum.IntEnum):
    """State machine states."""

    NEW = 0
    BZVALID = 1
    CIVALID = 2
    CIBZVALID = 3
    READYTOMERGE = 4
    MAINTOVERRIDE = 5
    UNKNOWN = 6


# pylint: disable=too-many-instance-attributes
class MRState:
    """Merge request state."""

    def __init__(self, lab, project, merge_request, payload):
        """Initialize the instance."""
        self.lab = lab
        self.project = project
        self.merge_request = merge_request
        self.payload = payload
        self.need_update = False
        # Set our initial state based on our bz and ci labels
        self.current_state = self.compute_label_state()
        self.next_state = State.UNKNOWN
        self.flags = {'bzreportlogs': False}
        self.mr_bugs = []
        self.deps = []

    def compute_label_state(self):
        """Compute the label state."""
        current_state = State.NEW
        dont_care_labels = ['maintainerOverride', 'readyForMerge',
                            'bzValidationFails', 'mrContainsDeps']
        labels_to_states = [
            {'label': set([]), 'state': State.NEW},
            {'label': set(['bzValidated']), 'state': State.BZVALID},
            {'label': set(['passesCI']), 'state': State.CIVALID},
            {'label': set(['bzValidated', 'passesCI']),
             'state': State.CIBZVALID}
        ]
        # make a copy of our labels
        valid_labels = self.merge_request.labels.copy()
        # remoe the labels that aren't relevant right now
        for i in dont_care_labels:
            try:
                valid_labels.remove(i)
            # pylint: disable=broad-except
            except Exception:
                pass

        # Find the initial state we are in
        for i in labels_to_states:
            if set(valid_labels) == i['label']:
                current_state = i['state']
                break

        # Now we need to make some adjustments based on maintainerOverride and
        # ReadyForMerge If the maintainer override flag has been applied, then
        # we are in MAINTOVERRIDE state
        if 'maintainerOverride' in self.merge_request.labels:
            current_state = State.MAINTOVERRIDE
        # If the readyForMerge flag is set, then we are in ReadyToMerge state
        if 'readyForMerge' in self.merge_request.labels:
            current_state = State.READYTOMERGE

        return current_state

    def add_label(self, label):
        """Add a label."""
        if label not in self.merge_request.labels:
            self.merge_request.labels.append(label)
            self.need_update = True

    def remove_label(self, label):
        """Remove a label."""
        if label in self.merge_request.labels:
            self.merge_request.labels.remove(label)
            self.need_update = True

    def add_if_not_label(self, label, iflabel):
        """Conditionally add a label."""
        if iflabel not in self.merge_request.labels:
            if label not in self.merge_request.labels:
                self.merge_request.labels.append(label)
                self.need_update = True

    def remove_if_not_label(self, label, iflabel):
        """Conditionally remove a label."""
        if iflabel not in self.merge_request.labels:
            if label in self.merge_request.labels:
                self.merge_request.labels.remove(label)
                self.need_update = True

    def update_mr_state(self):
        """Update the state machine state."""
        if self.next_state == State.NEW:
            self.remove_label('bzValidated')
            self.remove_label('passesCI')
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
        elif self.next_state == State.BZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('passesCI')
            self.add_label('bzValidated')
        elif self.next_state == State.CIVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.remove_label('bzValidated')
            self.add_label('passesCI')
        elif self.next_state == State.CIBZVALID:
            self.remove_if_not_label('readyForMerge', 'maintainerOverride')
            self.add_label('passesCI')
            self.add_label('bzValidated')
        elif self.next_state == State.READYTOMERGE:
            for label in self.merge_request.labels:
                if label.startswith("MR-NACK:"):
                    LOGGER.info(
                        "Skipping setting of readytomerge due to nacks\n")
                    return
            self.add_label('readyForMerge')
        elif self.next_state == State.MAINTOVERRIDE:
            self.add_label('maintainerOverride')

        if self.need_update:
            if misc.is_production():
                self.merge_request.save()
            else:
                LOGGER.info('Skipping merge request update in non-production')

    def force_mr_state(self, state):
        """Set the current state machine state."""
        self.current_state = state
        LOGGER.debug("Forcing MR state back to %s",
                     self.current_state)

    def update_current_state(self):
        """Update the current state machine state."""
        self.current_state = self.next_state
        LOGGER.debug("Updating MR current state to %s",
                     self.current_state)

    def compute_next_state(self):
        """Compute the next state machine state."""
        LOGGER.debug("Computing next state from state %s",
                     self.current_state)
        if self.current_state == State.NEW:
            self.__run_bz_validation()
            # need to move the state machine along so the ci state is correct
            self.update_current_state()
            self.__check_ci_result()
        elif self.current_state == State.BZVALID:
            self.__check_ci_result()
        elif self.current_state == State.CIVALID:
            self.__run_bz_validation()
        elif self.current_state == State.CIBZVALID:
            self.__check_approvals()
        elif self.current_state == State.READYTOMERGE:
            self.__check_approvals()
        elif self.current_state == State.MAINTOVERRIDE:
            self.__force_ready_to_merge()
        else:
            self.__log_unknown_state()

        LOGGER.debug("Computed next state as %s", self.next_state)

    def __check_ci_result(self):
        LOGGER.debug("Checking CI state\n")
        self.next_state = self.current_state
        if self.payload['object_kind'] == 'pipeline':
            # We're running this because a pipeline completed
            LOGGER.debug("pipeline state is %s",
                         self.payload['object_attributes']['status'])
            if self.payload['object_attributes']['status'] == 'success':
                if self.current_state == State.BZVALID:
                    self.next_state = State.CIBZVALID
                else:
                    self.next_state = State.CIVALID
        else:
            LOGGER.debug("Cehcking MR pipeline result\n")
            # We're running because we got an MR update
            if self.merge_request.pipeline:
                if self.merge_request.pipeline['status'] == 'success':
                    if self.current_state == State.BZVALID:
                        self.next_state = State.CIBZVALID
                    else:
                        self.next_state = State.CIVALID
            else:
                LOGGER.debug("Checking CI as MR with no pipeline!\n")
                # No relevant pipeline status, keep our current state
                self.next_state = self.current_state

        if self.next_state == State.CIBZVALID:
            self.update_current_state()
            self.__check_approvals()

    def __run_bz_validation(self):
        self.next_state = self.current_state
        LOGGER.info("Running bz validation\n")
        if 'bzValidationFails' in self.merge_request.labels:
            return
        LOGGER.debug("Merge request description:\n%s",
                     self.merge_request.description)
        self.mr_bugs = extract_bzs(self.merge_request.description)
        if len(self.mr_bugs) > 0:
            LOGGER.debug("Found MR bugs: %s", self.mr_bugs)
        self.deps = extract_dependencies(self.merge_request.description)
        if len(self.deps) > 0:
            LOGGER.debug("Found dependent bugs: %s", self.deps)
        (notes, bz_target, table, approved, mrdeps) = check_on_bzs(self)
        trim_notes(notes)
        if misc.is_production():
            report = print_gitlab_report(self, notes, bz_target, table,
                                         approved)
            self.merge_request.notes.create({'body': report})
        else:
            LOGGER.info('Skipping adding BZ report in non-production')
            report = print_text_report(self.merge_request, notes, bz_target,
                                       table, approved)
            LOGGER.debug(report)
        if approved:
            if self.current_state == State.CIVALID:
                self.next_state = State.CIBZVALID
            else:
                self.next_state = State.BZVALID
        else:
            LOGGER.info("WE FAILED BZ VALIDATION!  MARK US AS SUCH!\n")
            # This is not really part of the state machine, but rather a marker
            # to prevent us from having to create several more states to
            # consider
            self.merge_request.labels.append('bzValidationFails')
            if mrdeps:
                LOGGER.info("MR contains dependencies still! Add label...\n")
                self.merge_request.labels.append('mrContainsDeps')
            self.need_update = True
        if self.next_state == State.CIBZVALID:
            self.update_current_state()
            self.__check_approvals()

    def __check_approvals(self):
        approvals = self.merge_request.approvals.get()
        LOGGER.info("We have %s approvals", len(approvals.approved_by))
        if len(approvals.approved_by) >= 1:
            if self.current_state == State.CIBZVALID:
                # This MR is ready for merging
                LOGGER.debug("Marking MR as ready")
                self.next_state = State.READYTOMERGE
            elif self.current_state == State.READYTOMERGE:
                self.next_state = State.READYTOMERGE
        else:
            if self.current_state == State.READYTOMERGE:
                if 'bzValidated' in self.merge_request.labels:
                    self.next_state = State.BZVALID
                    if 'passesCI' in self.merge_request.labels:
                        self.next_state = State.CIBZVALID
                elif 'passesCI' in self.merge_request.labels:
                    self.next_state = State.CIVALID
            else:
                self.next_state = self.current_state

    def __do_nothing(self):
        self.next_state = self.current_state

    def __force_ready_to_merge(self):
        self.next_state = State.READYTOMERGE

    def __log_unknown_state(self):
        LOGGER.warning("MR %s IS IN UNKOWN STATE", self.merge_request.iid)


def get_mrstate(gl_instance, message, key):
    """Return a merge request state for the webhook payload."""
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(message.payload[key]["iid"])
    return MRState(gl_instance, gl_project, gl_mergerequest, message.payload)


def process_mr(gl_instance, message):
    """Process a merge request message."""
    mrstate = get_mrstate(gl_instance, message, "object_attributes")
    if mrstate:
        mrstate.compute_next_state()
        mrstate.update_mr_state()


def process_note(gl_instance, message):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    mrstate = get_mrstate(gl_instance, message, "merge_request")
    notetext = message.payload['object_attributes']['note']

    if notetext.startswith("request-bz-evaluation"):
        if notetext == "request-bz-evaluation(logs)":
            LOGGER.info("Bz report requests full logs")
            mrstate.flags['bzreportlogs'] = True
        elif notetext != "request-bz-evaluation":
            # Do nothing if the text is not either of the above
            # 2 variants
            return

        # Force a re-run of the bz validation
        try:
            LOGGER.debug("Removing bzValidationFails and mrContainsDeps\n")
            mrstate.merge_request.labels.remove('bzValidationFails')
            mrstate.merge_request.labels.remove('mrContainsDeps')
        # pylint: disable=broad-except
        except Exception:
            pass
        mrstate.force_mr_state(State.NEW)
    elif notetext == "request-pipeline-evaluation":
        mrstate.force_mr_state(State.BZVALID)
    else:
        # This is a note we can ignore, just return here
        return

    if mrstate:
        mrstate.compute_next_state()
        mrstate.update_mr_state()


def process_pipeline(gl_instance, message):
    """Process a pipeline message."""
    LOGGER.debug("Checking pipeline request\n")
    # We don't care about this event if there isn't an associated merge request
    if not message.payload['merge_request']:
        return

    mrstate = get_mrstate(gl_instance, message, "merge_request")

    # Force our state back to a state that requires CI checking
    if mrstate.current_state == State.CIVALID:
        mrstate.force_mr_state(State.NEW)
    elif mrstate.current_state == State.CIBZVALID:
        mrstate.force_mr_state(State.BZVALID)

    if mrstate:
        mrstate.compute_next_state()
        mrstate.update_mr_state()


WEBHOOKS = {
    "pipeline": process_pipeline,
    "merge_request": process_mr,
    "note": process_note,
}


def process_note_url(mr_url, note):
    """Process the given note for a merge request url."""
    payload = common.make_payload(mr_url, "note")
    payload["object_attributes"]["note"] = note
    payload["merge_request"] = {}
    payload["merge_request"]["iid"] = payload["_mr_id"]
    del payload["_mr_id"]
    common.process_message('cmdline', payload, WEBHOOKS)


def process_merge_request_url(mr_url):
    """Process a merge request url."""
    payload = common.make_payload(mr_url, "merge_request")
    payload["object_attributes"]["iid"] = payload["_mr_id"]
    del payload["_mr_id"]
    common.process_message('cmdline', payload, WEBHOOKS)


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGZILLA')
    parser.add_argument('--note',
                        help='Process a note for the given merge request')
    args = parser.parse_args(args)
    if args.merge_request and args.note:
        process_note_url(args.merge_request, args.note)
        return
    if args.merge_request:
        process_merge_request_url(args.merge_request)
        return

    common.consume_queue_messages(args, LOGGER, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
