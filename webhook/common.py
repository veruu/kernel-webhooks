"""Common code that can be used by all webhooks."""
import argparse
import os
import re
import sys
from urllib import parse

from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.messagequeue import Message
from cki_lib.messagequeue import MessageQueue
import sentry_sdk


def get_arg_parser(webhook_prefix):
    """Intialize a commandline parser.

    Returns: argparse parser.
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')

    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--rabbitmq-host', default=os.environ.get('RABBITMQ_HOST', 'localhost'))
    parser.add_argument('--rabbitmq-port', type=int,
                        default=misc.get_env_int('RABBITMQ_PORT', 5672))
    parser.add_argument('--rabbitmq-user', default=os.environ.get('RABBITMQ_USER', 'guest'))
    parser.add_argument('--rabbitmq-password',
                        default=os.environ.get('RABBITMQ_PASSWORD', 'guest'))
    parser.add_argument('--rabbitmq-exchange',
                        default=os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.webhooks'))
    parser.add_argument('--rabbitmq-routing-key',
                        default=os.environ.get(f'{webhook_prefix}_ROUTING_KEYS'),
                        help='RabbitMQ routing key. Required when processing queue.')
    parser.add_argument('--rabbitmq-queue-name',
                        default=os.environ.get(f'{webhook_prefix}_QUEUE'),
                        help='RabbitMQ queue name. Required when processing queue.')

    return parser


def parse_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_mergerequest, project_path


# pylint: disable=unused-argument
def process_message(routing_key, payload, webhooks):
    """Process a webhook message."""
    object_kind = payload.get('object_kind')
    if not object_kind:
        return False  # unit tests
    if object_kind not in webhooks:
        return False  # unit tests

    message = Message(payload)
    with message.gl_instance() as gl_instance:
        webhooks[object_kind](gl_instance, message)

    return True  # unit tests


def consume_queue_messages(args, logger, webhooks):
    """Begin processing the main loop by reading messages from the queue."""
    def _queue_processor(routing_key, payload):
        logger.info('Processing message from queue: %s', payload)
        process_message(routing_key, payload, webhooks)

    if not args.rabbitmq_queue_name or not args.rabbitmq_routing_key:
        logger.error('The arguments --rabbitmq-queue-name and --rabbitmq-routing-key must be ' +
                     'specified in order to process the queue. Hint: You may want to process ' +
                     'a single merge request with the --merge-request argument.')
        sys.exit(1)

    queue = MessageQueue(args.rabbitmq_host, args.rabbitmq_port, args.rabbitmq_user,
                         args.rabbitmq_password)

    if misc.is_production():
        sentry_sdk.init(ca_certs=args.sentry_ca_certs)

    queue.consume_messages(args.rabbitmq_exchange, args.rabbitmq_routing_key.split(),
                           _queue_processor, args.rabbitmq_queue_name)


def get_argparse_environ_opts(key):
    """Read default value for argparse from an environment variable if present."""
    val = os.environ.get(key)
    return {'default': val} if val else {'required': True}


def extract_files(commit, logger):
    """Extract the list of files from the commit."""
    filelist = []
    diffs = commit.diff()
    logger.debug(diffs)
    for diff in diffs:
        logger.debug(diff)
        filelist.append(diff.new_path)
    logger.debug(filelist)
    return filelist


def print_notes(notes):
    """Print the notes section of upstream commit ID report."""
    report = ""
    noteid = 0
    while noteid < len(notes):
        report += f"{noteid + 1}. "
        if notes[noteid] is None:
            noteid += 1
            continue
        report += notes[noteid].rstrip("\n").replace("\n", "\n   ")
        report += "\n"
        noteid += 1
    report += "\n" if report else ""
    report = report.replace("<", "&lt;")
    report = report.replace(">", "&gt;")
    report = report.replace("\n   ", "<br>&emsp;")
    return report


def make_payload(args, kind):
    """Create a fake payload dict."""
    payload = {'object_kind': kind,
               'object_attributes': {},
               'project': {},
               '_mr_id': None}
    url_parts = parse.urlsplit(args.merge_request)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    prj_id = re.sub('/-$', '', match[1])
    payload["project"]["id"] = prj_id
    web_url = "%s://%s/%s" % (url_parts.scheme, url_parts.netloc, prj_id)
    payload["project"]["web_url"] = web_url
    payload["_mr_id"] = int(match[2])
    return payload


def build_note_string(notes):
    """Build note string for report table."""
    notestr = ", ".join(notes)
    notestr = "See " + notestr + "|\n" if notestr else "-|\n"
    return notestr


def build_commits_for_row(row):
    """Build list of commits for a row in report table."""
    commits = row[1] if len(row[1]) < 2 else row[1][:2] + ["(...)"]
    count = 0
    while count < len(commits):
        commits[count] = commits[count][:8]
        count += 1
    return commits
