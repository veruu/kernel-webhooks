"""Public merge request handling."""
import json
import os
import sys

from cki_lib import logger
from cki_lib import misc

from . import common

CONFIG_LABEL = "Configuration"
NO_CCS_LABEL = "No CCs"

LOGGER = logger.get_logger('cki.webhook.public')


def _save(gl_mergerequest):
    if misc.is_production():
        gl_mergerequest.save()
    else:
        LOGGER.info('Skipping merge request update in non-production')
        updated_data = "Merge request data changes:"
        if '_updated_attrs' in gl_mergerequest.__dict__:
            attrs = gl_mergerequest.__dict__['_updated_attrs']
            changed = json.dumps(attrs, indent=2)
            updated_data = "%s\n%s" % (updated_data, changed)
        else:
            updated_data += " None"
        LOGGER.debug(updated_data)


def _drop_ack_nack_labels(gl_mergerequest, gl_action, gl_oldrev):
    """If the merge request got updated, clear any Acked-by/Nacked-by tags."""
    if gl_action != "update":
        LOGGER.debug("Skipping clearing acks/nacks since %r isn't updated",
                     gl_mergerequest)
        return
    if not gl_oldrev:
        LOGGER.debug(
            "Merge request %r got updated, but no code changes were made so "
            "ack/naks are still valid",
            gl_mergerequest,
        )
        return

    gl_mergerequest.labels = [
        label for label in gl_mergerequest.labels
        if "Acked-by" not in label and "Nacked-by" not in label]
    _save(gl_mergerequest)


def _apply_subsystem_label(gl_project, gl_mergerequest):
    """Apply a subsystem label to configuration merge requests.

    This is intended to run in a thread as it can take a long time to perform a
    code search and web hooks need to respond quickly. It's possible we'll miss
    labeling some merge requests if the process is killed after acking the web
    hook and before finishing this, but it's not a big deal.

    An entirely new GitLab client is created as the underlying requests session
    is not thread-safe.
    """
    subsystem_labels = set()
    for change in gl_mergerequest.changes()["changes"]:
        file_name = os.path.basename(change["new_path"])
        if file_name.startswith("CONFIG_"):
            results = gl_project.search("blobs", f"config {file_name[7:]}")
            for path in [
                    os.path.dirname(p["path"]) for p in results
                    if "Kconfig" in p["path"]]:
                # if drivers/net/ethernet/ do 4 parts at most
                # otherwise do 2 parts at most
                if path.startswith("drivers/net/ethernet/"):
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split(
                            "/")[:4]])
                else:
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split(
                            "/")[:2]])

    if subsystem_labels:
        gl_mergerequest.labels += list(subsystem_labels)
        _save(gl_mergerequest)


def _apply_config_label(gl_mergerequest) -> None:
    """Add or remove the CONFIG_LABEL to merge requests."""
    config_dirs = [
        f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]
    labels = [label for label in gl_mergerequest.labels
              if label not in (CONFIG_LABEL, NO_CCS_LABEL)]

    for change in gl_mergerequest.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                    "new_path"
            ].startswith(config_dir):
                labels.append(CONFIG_LABEL)
                for commit in gl_mergerequest.commits():
                    if not commit.message or 'Cc: ' not in commit.message:
                        labels.append(NO_CCS_LABEL)

    # This also strips stale Configuration labels
    if labels != gl_mergerequest.labels:
        gl_mergerequest.labels = labels
        _save(gl_mergerequest)


def process_merge_request(gl_mergerequest, gl_action, gl_oldrev):
    """Process a merge request."""
    _apply_config_label(gl_mergerequest)
    # _apply_subsystem_label(gl_project, gl_mergerequest)
    _drop_ack_nack_labels(gl_mergerequest, gl_action, gl_oldrev)


def process_merge_request_webhook(gl_instance, message):
    """Process a merge request message.

    Web hook for when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch

    The request body is a JSON document documented at
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
    """
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(
        message.payload["object_attributes"]["iid"])
    gl_action = message.payload["object_attributes"].get('action', '')
    gl_oldrev = "oldrev" in message.payload["object_attributes"]
    process_merge_request(gl_mergerequest, gl_action, gl_oldrev)


# The set of web hook handlers that are currently supported. Consult `Gitlab
# webhooks`_ documentation for complete list of webhooks and the payload
# details.
#
# The key should be the value GitLab uses in the "object_kind" parameter in
# the payload of the web hook request.
#
# .. _Gitlab webhooks:
# https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
WEBHOOKS = {
    "merge_request": process_merge_request_webhook,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('PUBLIC')
    args = parser.parse_args(args)
    if args.merge_request:
        _, gl_mr, _ = common.parse_mr_url(args.merge_request)
        process_merge_request(gl_mr, args.action, args.oldrev)
        return

    common.consume_queue_messages(args, LOGGER, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
